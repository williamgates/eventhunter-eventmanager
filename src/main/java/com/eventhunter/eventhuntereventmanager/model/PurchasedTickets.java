package com.eventhunter.eventhuntereventmanager.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "purchasedtickets")
public class PurchasedTickets implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "username")
    private String userName;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "eventsName")
    private String eventsName;

    protected PurchasedTickets() {
    }

    public PurchasedTickets(String userName, int quantity, String eventsName) {
        this.userName = userName;
        this.quantity = quantity;
        this.eventsName = eventsName;
    }

    public long getId() {
        return id;
    }

    public String getEventsName() { return eventsName; }



    public String getUserName() {
        return userName;
    }



    public int getQuantity() { return quantity; }




}
