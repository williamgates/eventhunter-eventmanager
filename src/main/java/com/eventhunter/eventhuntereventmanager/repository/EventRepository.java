package com.eventhunter.eventhuntereventmanager.repository;

import com.eventhunter.eventhuntereventmanager.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EventRepository extends JpaRepository<Event, Long> {
    Event findById(long id);
    List<Event> findByManagerUsername(String managerUsername);
    List<Event> findByTitle(String title);
    boolean existsById(long id);
    boolean existsByTitle(String title);
    void deleteById(long id);
}
