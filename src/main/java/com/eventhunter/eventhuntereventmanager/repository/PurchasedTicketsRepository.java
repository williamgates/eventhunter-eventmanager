package com.eventhunter.eventhuntereventmanager.repository;

import com.eventhunter.eventhuntereventmanager.model.PurchasedTickets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchasedTicketsRepository extends JpaRepository<PurchasedTickets, Long> {
    PurchasedTickets findById(long id);
    List<PurchasedTickets> findByUserName(String userName);
    List<PurchasedTickets> findByEventsName(String eventsName);
    boolean existsById(long id);

}
