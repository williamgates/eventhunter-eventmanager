package com.eventhunter.eventhuntereventmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventhunterEventmanagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventhunterEventmanagerApplication.class, args);
    }

}
