package com.eventhunter.eventhuntereventmanager.controller;

import com.eventhunter.eventhuntereventmanager.form.CreateEventForm;
import com.eventhunter.eventhuntereventmanager.form.DeleteEventForm;
import com.eventhunter.eventhuntereventmanager.form.PurchaseTicketForm;
import com.eventhunter.eventhuntereventmanager.model.Event;
import com.eventhunter.eventhuntereventmanager.model.PurchasedTickets;
import com.eventhunter.eventhuntereventmanager.repository.EventRepository;
import com.eventhunter.eventhuntereventmanager.repository.PurchasedTicketsRepository;
import com.eventhunter.eventhuntereventmanager.security.EventHunterSecurity;
import org.hibernate.event.spi.DeleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@CrossOrigin
@RestController
@RequestMapping("/event")
public class EventController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    PurchasedTicketsRepository purchasedTicketsRepository;

    @GetMapping("/details/{id}")
    public Map<String, String> details(@PathVariable long id) {
        Map<String, String> map = new HashMap<>();
        if(!eventRepository.existsById(id)) {
            return map;
        }
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<HashMap<String, String>> response;
        String uri;
        Event event = eventRepository.findById(id);
        map.put("managerUsername", event.getManagerUsername());
        uri = "https://eventhunter-authentication.herokuapp.com/user/details/" + event.getManagerUsername();
        response = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<HashMap<String, String>>(){});
        HashMap<String, String> userDetails = response.getBody();
        map.put("managerName", userDetails.get("name"));
        map.put("title", event.getTitle());
        map.put("category", event.getCategory());
        map.put("location", event.getLocation());
        map.put("startTime", event.getStartTime().toString());
        map.put("endTime", event.getEndTime().toString());
        map.put("imageUrl", event.getImageUrl());
        map.put("totalTickets", Integer.toString(event.getTotalTickets()));
        return map;
    }

    @GetMapping("/get-all")
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @GetMapping("/get-managed-events")
    public List<Event> getManagedEvents(@RequestHeader(value = "Authorization", defaultValue = "") String token) {
        String managerUsername = EventHunterSecurity.getUsername(token);
        return eventRepository.findByManagerUsername(managerUsername);
    }

    @PostMapping("/create")
    public ResponseEntity create(@RequestHeader(value = "Authorization", defaultValue = "") String token,
                                 @RequestBody CreateEventForm createEventForm) {
        if (!EventHunterSecurity.verifyAuthentication(token)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
        if (createEventForm.getTitle() == null || createEventForm.getCategory() == null ||
                createEventForm.getLocation() == null || createEventForm.getStartTime() == null ||
                createEventForm.getEndTime() == null || createEventForm.getImageUrl() == null ||
                createEventForm.getTotalTickets() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("All fields cannot be empty.");
        }
        String managerUsername = EventHunterSecurity.getUsername(token);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        LocalDateTime startTime;
        LocalDateTime endTime;
        try {
            startTime = LocalDateTime.parse(createEventForm.getStartTime(), formatter);
            endTime = LocalDateTime.parse(createEventForm.getEndTime(), formatter);
        }
        catch (DateTimeParseException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Wrong start time or end time format.");
        }
        if (eventRepository.existsByTitle(createEventForm.getTitle())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Event with that title already exists.");
        }
        if (startTime.compareTo(LocalDateTime.now()) < 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Start time should be greater than current time.");
        }
        if (endTime.compareTo(startTime) < 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("End time should be greater than start time.");
        }
        if (createEventForm.getTotalTickets() <= 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tickets to be sold must be greater than 0.");
        }
        Event event = new Event(managerUsername,
                createEventForm.getTitle(),
                createEventForm.getCategory(),
                createEventForm.getLocation(),
                startTime,
                endTime,
                createEventForm.getImageUrl(),
                createEventForm.getTotalTickets());
        eventRepository.save(event);
        return ResponseEntity.ok().body(null);
    }

    @PostMapping("/delete")
    public ResponseEntity delete(@RequestHeader(value = "Authorization", defaultValue = "") String token,
                                 @RequestBody DeleteEventForm deleteEventForm) {
        if (!EventHunterSecurity.verifyAuthentication(token)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
        if (deleteEventForm.getId() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Event ID cannot be empty.");
        }
        if (!eventRepository.existsById(deleteEventForm.getId())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Cannot find requested event.");
        }
        String username = EventHunterSecurity.getUsername(token);
        Event event = eventRepository.findById(deleteEventForm.getId()).get();
        if (!event.getManagerUsername().equals(username)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("This user is not allowed to delete this event");
        }
        String title = event.getTitle();
        List<PurchasedTickets> purchasedTicketsList = purchasedTicketsRepository.findByEventsName(title);
        for (PurchasedTickets purchasedTickets : purchasedTicketsList) {
            purchasedTicketsRepository.delete(purchasedTickets);
        }
        eventRepository.deleteById(deleteEventForm.getId());
        return ResponseEntity.ok().body(null);
    }

    private Lock purchaseTicketLock = new ReentrantLock();
    @PostMapping("/purchase-ticket")
    public ResponseEntity purchaseTicket(@RequestHeader(value = "Authorization", defaultValue = "") String token,
                                         @RequestBody PurchaseTicketForm purchaseTicketForm) {
        if (!EventHunterSecurity.verifyAuthentication(token)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
        if (purchaseTicketForm.getId() == null ||  purchaseTicketForm.getAmount() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incomplete requests.");
        }
        if (!eventRepository.existsById(purchaseTicketForm.getId())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Cannot find requested event.");
        }
        if (purchaseTicketForm.getAmount() < 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Purchase amount must be greater than 0.");
        }
        purchaseTicketLock.lock();
        try {
            Event event = eventRepository.findById(purchaseTicketForm.getId()).get();
            if (purchaseTicketForm.getAmount() > event.getTotalTickets()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Not enough tickets left.");
            }
            event.setTotalTickets(event.getTotalTickets() - purchaseTicketForm.getAmount());
            eventRepository.save(event);
            messagingTemplate.convertAndSend("/notification/" + event.getId() + "/",
                    purchaseTicketForm.getAmount() + " ticket(s) sold just now!");
        }
        finally {
            purchaseTicketLock.unlock();
        }
        return ResponseEntity.ok().body(null);
    }
}
