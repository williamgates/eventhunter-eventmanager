package com.eventhunter.eventhuntereventmanager.controller;

import com.eventhunter.eventhuntereventmanager.model.Event;
import com.eventhunter.eventhuntereventmanager.model.PurchasedTickets;
import com.eventhunter.eventhuntereventmanager.repository.EventRepository;
import com.eventhunter.eventhuntereventmanager.repository.PurchasedTicketsRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import com.eventhunter.eventhuntereventmanager.security.EventHunterSecurity;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@CrossOrigin
@RestController
@RequestMapping("/purchased-tickets")
public class PurchasedTicketsController {
    @Autowired
    EventRepository eventRepository;

    @Autowired
    PurchasedTicketsRepository purchasedTicketsRepository;

    @GetMapping("/get-all")
    public List<PurchasedTickets> getAllPurchasedTickets() {
        return purchasedTicketsRepository.findAll();
    }


    @GetMapping("/details/{id}")
    public Map<String, String> details(@PathVariable long id) {
        Map<String, String> map = new HashMap<>();
        if (!purchasedTicketsRepository.existsById(id)) {
            return map;
        }
        PurchasedTickets tickets = purchasedTicketsRepository.findById(id);
        map.put("userName", tickets.getUserName());
        map.put("eventsName", tickets.getEventsName());
        map.put("quantity", Integer.toString(tickets.getQuantity()));
        return map;
    }

    @PostMapping("/create")
    public ResponseEntity create(@RequestHeader(value = "Authorization", defaultValue = "") String token, @RequestParam("user-name") String userName, @RequestParam("quantity") int quantity, @RequestParam("events-name") String eventsName) {
        if (!EventHunterSecurity.verifyAuthentication(token)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        } else {
            PurchasedTickets purchasedTickets = new PurchasedTickets(userName, quantity, eventsName);
            purchasedTicketsRepository.save(purchasedTickets);
        }

        return ResponseEntity.ok().body(null);
    }

    private Lock cancelTicketLock = new ReentrantLock();
    @PostMapping("/delete")
    public ResponseEntity delete(@RequestHeader(value = "Authorization", defaultValue = "") String token, @RequestParam("id") long id) {
        int canceledAmount;
        String eventTitle;
        if (!EventHunterSecurity.verifyAuthentication(token)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        } else {
            PurchasedTickets tickets = purchasedTicketsRepository.findById(id);
            canceledAmount = tickets.getQuantity();
            eventTitle = tickets.getEventsName();
            purchasedTicketsRepository.delete(tickets);
        }
        cancelTicketLock.lock();
        try {
            Event event = eventRepository.findByTitle(eventTitle).get(0);
            event.setTotalTickets(event.getTotalTickets() + canceledAmount);
            eventRepository.save(event);
        }
        finally {
            cancelTicketLock.unlock();
        }
        return ResponseEntity.ok().body(null);
    }
}
