package com.eventhunter.eventhuntereventmanager.security;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

public class EventHunterSecurity {
    public static String createJwt(String username) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        long expMillis = nowMillis + SecurityConstants.EXPIRATION_TIME;
        Date exp = new Date(expMillis);
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SecurityConstants.SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        String jwt = Jwts.builder()
                .setSubject(username)
                .setIssuer(SecurityConstants.ISSUER)
                .setIssuedAt(now)
                .setExpiration(exp)
                .signWith(signatureAlgorithm, signingKey)
                .compact();
        return jwt;
    }

    public static Claims decodeJwt(String jwt) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(SecurityConstants.SECRET_KEY))
                    .parseClaimsJws(jwt).getBody();
            return claims;
        }
        catch (Exception e) {
            return null;
        }
    }

    public static String getUsername(String token) {
        try {
            Claims claims = decodeJwt(token.substring(7));
            System.out.println(claims.getSubject());
            return claims.getSubject();
        }
        catch (Exception e) {
            return null;
        }
    }

    public static boolean verifyAuthentication(String token) {
        try {
            long nowMillis = System.currentTimeMillis();
            Date now = new Date(nowMillis);
            Claims claims = decodeJwt(token.substring(7));
            return (claims.getIssuer().equals(SecurityConstants.ISSUER) && claims.getExpiration().compareTo(now) > 0);
        }
        catch (Exception e) {
            return false;
        }
    }
}
