package com.eventhunter.eventhuntereventmanager.form;

public class PurchaseTicketForm {
    private Long id;
    private Integer amount;

    public Long getId() {
        return id;
    }

    public Integer getAmount() {
        return amount;
    }
}
