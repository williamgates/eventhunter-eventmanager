package com.eventhunter.eventhuntereventmanager.form;

public class CreateEventForm {
    private String title;
    private String category;
    private String location;
    private String startTime;
    private String endTime;
    private String imageUrl;
    private Integer totalTickets;

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getLocation() {
        return location;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Integer getTotalTickets() {
        return totalTickets;
    }
}
