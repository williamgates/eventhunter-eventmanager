package com.eventhunter.eventhuntereventmanager;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class EventhunterMappingTests {
    @Autowired
    private MockMvc mvc;

    @Test
    public void pageNotFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/404"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void findAllPurchasedTickets() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/purchased-tickets/get-all")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }
    @Test
    public void findTicketsById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/purchased-tickets/details/1")
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }


}
