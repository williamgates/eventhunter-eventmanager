package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerModelTests;

import com.eventhunter.eventhuntereventmanager.model.Event;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EventTests {
    @Test
    public void testGetterSetter() throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        String managerUsername = "user";
        String title = "Title";
        String category = "Category";
        String location = "Location";
        LocalDateTime startTime = LocalDateTime.parse("2020-10-10"+'T'+"00:00", formatter);
        LocalDateTime endTime = LocalDateTime.parse("2020-10-10"+'T'+"00:00", formatter);
        String imageUrl = "";
        int totalTickets = 100;
        Event event = new Event(managerUsername, title, category, location, 
        startTime, endTime, imageUrl, totalTickets);

        Assert.assertTrue(event.getId() >= 0);
        Assert.assertEquals(managerUsername, event.getManagerUsername());
        Assert.assertEquals(title, event.getTitle());
        Assert.assertEquals(category, event.getCategory());
        Assert.assertEquals(location, event.getLocation());
        Assert.assertEquals(startTime, event.getStartTime());
        Assert.assertEquals(endTime, event.getEndTime());
        Assert.assertEquals(imageUrl, event.getImageUrl());
        Assert.assertEquals(totalTickets, event.getTotalTickets());

        String newImageUrl = "https://dummyimage.com/1080x1080.png/dddddd/000000";
        int newTotalTickets = 100;

        event.setImageUrl(newImageUrl);
        event.setTotalTickets(newTotalTickets);
        Assert.assertEquals(newImageUrl, event.getImageUrl());
        Assert.assertEquals(newTotalTickets, event.getTotalTickets());
    }
}
