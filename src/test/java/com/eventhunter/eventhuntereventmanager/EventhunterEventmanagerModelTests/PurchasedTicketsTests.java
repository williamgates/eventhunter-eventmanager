package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerModelTests;

import com.eventhunter.eventhuntereventmanager.model.PurchasedTickets;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;




@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchasedTicketsTests {
    @Test
    public void testGetterSetter() throws Exception {
        String username = "user";
        int quantity = 1;
        String eventsName = "Sample Title";
        PurchasedTickets purchasedTickets = new PurchasedTickets(username,quantity,eventsName);

        Assert.assertTrue(purchasedTickets.getId() >= 0);
        Assert.assertEquals(username, purchasedTickets.getUserName());
        Assert.assertEquals(eventsName, purchasedTickets.getEventsName());
        Assert.assertEquals(quantity, purchasedTickets.getQuantity());

    }
}
