package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerBeforeTests;

import com.eventhunter.eventhuntereventmanager.model.Event;
import com.eventhunter.eventhuntereventmanager.repository.EventRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class BeforeTests{
    @Autowired
    private EventRepository eventRepository;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private static boolean dataLoaded = false;
    @Before
    public void setUp(){
        if (!dataLoaded){
            Event event = new Event(
                "user",
                "Sample Event",
                "Sample Category",
                "Sample Location",
                LocalDateTime.parse("2020-10-10 19:00", formatter),
                LocalDateTime.parse("2020-10-10 20:00", formatter),
                "",
                100
            );
            eventRepository.save(event);
            dataLoaded = true;
        }
    }

    @Test
    public void contextLoads(){
    }
}
