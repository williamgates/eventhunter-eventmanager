package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerSecurityTests;

import com.eventhunter.eventhuntereventmanager.security.EventHunterSecurity;
import com.eventhunter.eventhuntereventmanager.security.SecurityConstants;
import io.jsonwebtoken.Claims;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SecurityTests {
    @Test
    public void testSecurityConstants() throws Exception {
        SecurityConstants securityConstants = new SecurityConstants();
        Assert.assertNotNull(securityConstants);
    }

    @Test
    public void testSuccessSecurity() throws Exception {
        String username = "user";
        String jwt = EventHunterSecurity.createJwt(username);
        String token = "Bearer " + jwt;
        Claims claims = EventHunterSecurity.decodeJwt(jwt);
        Assert.assertEquals(claims.getSubject(), username);
        Assert.assertEquals(EventHunterSecurity.getUsername(token), username);
        Assert.assertTrue(EventHunterSecurity.verifyAuthentication(token));
    }

    @Test
    public void testFailSecurity() throws Exception {
        EventHunterSecurity.decodeJwt(null);
        EventHunterSecurity.getUsername(null);
        EventHunterSecurity.verifyAuthentication(null);
    }

}
