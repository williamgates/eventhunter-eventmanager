package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerEventControllerTests;

import com.eventhunter.eventhuntereventmanager.repository.EventRepository;
import com.eventhunter.eventhuntereventmanager.security.EventHunterSecurity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class DeleteEventTests {
    @Autowired
    private MockMvc mvc;

    @Autowired
    EventRepository eventRepository;

    @Test
    public void testUnauthorized() throws Exception {
        mvc.perform(post("/event/delete").contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testBadRequest() throws Exception {
        String token = "Bearer " + EventHunterSecurity.createJwt("user");
        mvc.perform(post("/event/delete").contentType(APPLICATION_JSON_UTF8)
                .header("Authorization", token))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testSuccess() throws Exception {
        String token = "Bearer " + EventHunterSecurity.createJwt("user");
        mvc.perform(post("/event/delete").contentType(APPLICATION_JSON_UTF8)
                .header("Authorization", token)
                .content("{\"id\": \"1\"}"))
                .andExpect(status().isOk());
    }
}
