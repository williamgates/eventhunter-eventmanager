package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerEventControllerTests;

import com.eventhunter.eventhuntereventmanager.repository.EventRepository;
import com.eventhunter.eventhuntereventmanager.security.EventHunterSecurity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class CreateEventTests {
    @Autowired
    private MockMvc mvc;

    @Autowired
    EventRepository eventRepository;

    @Test
    public void testCreateEvent() throws Exception {
        Map<String, String> map = new HashMap<>();
        String token = "Bearer " + EventHunterSecurity.createJwt("user");
        map.put("title", "Sample Title9");
        map.put("category", "Sample Category");
        map.put("location", "Sample Location");
        map.put("startTime", "2020-01-01T19:00");
        map.put("endTime", "2020-01-01T20:00");
        map.put("imageUrl", "http://example.com");
        map.put("totalTickets", "100");
        mvc.perform(post("/event/create").contentType(APPLICATION_JSON_UTF8)
                .header("Authorization", token)
                .content(new ObjectMapper().writeValueAsString(map)))
                .andExpect(status().isOk());
    }
}
