package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerEventControllerTests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.eventhunter.eventhuntereventmanager.model.Event;
import com.eventhunter.eventhuntereventmanager.model.PurchasedTickets;

import com.eventhunter.eventhuntereventmanager.repository.EventRepository;
import com.eventhunter.eventhuntereventmanager.repository.PurchasedTicketsRepository;
import com.eventhunter.eventhuntereventmanager.security.EventHunterSecurity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.xml.stream.util.EventReaderDelegate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class PurchasedTicketsControllerTests {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private PurchasedTicketsRepository purchasedTicketsRepository;

    @Autowired
    private EventRepository eventRepository;

    @Test
    public void createPurchasedTickets() throws Exception {
        String token =  "Bearer " + EventHunterSecurity.createJwt("user");


        mvc.perform(post("/purchased-tickets/create")
                .param("user-name", "ady")
                .param("quantity", "5")
                .param("events-name", "Tokopedia")
                .header("Authorization", token))
                .andExpect(status().isOk());

        PurchasedTickets createdPurchasedTickets = purchasedTicketsRepository.findAll().get(0);
        Assert.assertEquals(createdPurchasedTickets.getUserName(), "ady");
        Assert.assertEquals(createdPurchasedTickets.getQuantity(), Integer.parseInt("5"));
        Assert.assertEquals(createdPurchasedTickets.getEventsName(), "Tokopedia");
    }
    @Test
    public void deletePurchasedTickets() throws Exception {
        String token =  "Bearer " + EventHunterSecurity.createJwt("newuser");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        Event event = new Event(
                "user",
                "Sample Event22",
                "Sample Category",
                "Sample Location",
                LocalDateTime.parse("2020-10-10 19:00", formatter),
                LocalDateTime.parse("2020-10-10 20:00", formatter),
                "",
                100
        );
        eventRepository.save(event);

        mvc.perform(post("/purchased-tickets/create")
                .param("user-name", "ady")
                .param("quantity", "5")
                .param("events-name", "Sample Event22")
                .header("Authorization", token))
                .andExpect(status().isOk());

        PurchasedTickets purchasedTickets = purchasedTicketsRepository.findByEventsName("Sample Event22").get(0);

        mvc.perform(post("/purchased-tickets/delete")
                .param("id", Long.toString(purchasedTickets.getId()))
                .header("Authorization", token))
                .andExpect(status().isOk());
    }

}
