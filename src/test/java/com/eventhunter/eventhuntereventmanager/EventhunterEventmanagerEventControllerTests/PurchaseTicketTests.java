package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerEventControllerTests;

import com.eventhunter.eventhuntereventmanager.model.Event;
import com.eventhunter.eventhuntereventmanager.repository.EventRepository;
import com.eventhunter.eventhuntereventmanager.security.EventHunterSecurity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class PurchaseTicketTests {
    @Autowired
    private MockMvc mvc;

    @Autowired
    EventRepository eventRepository;

    @Test
    public void testUnauthorized() throws Exception {
        mvc.perform(post("/event/purchase-ticket").contentType(APPLICATION_JSON_UTF8)
                .content("{\"id\":\"1\",\"amount\":\"10\"}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testSuccess() throws Exception {
        String token = "Bearer " + EventHunterSecurity.createJwt("user");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        Event event = new Event(
                "manager",
                "Sample Event8",
                "Sample Category",
                "Sample Location",
                LocalDateTime.parse("2020-10-10 19:00", formatter),
                LocalDateTime.parse("2020-10-10 20:00", formatter),
                "",
                100
        );
        eventRepository.save(event);
        event = eventRepository.findByTitle("Sample Event8").get(0);
        mvc.perform(post("/event/purchase-ticket").contentType(APPLICATION_JSON_UTF8)
                .header("Authorization", token)
                .content("{\"id\":\"" + event.getId() + "\",\"amount\":\"10\"}"))
                .andExpect(status().isOk());
    }
}
