package com.eventhunter.eventhuntereventmanager.EventhunterEventmanagerEventControllerTests;

import com.eventhunter.eventhuntereventmanager.repository.EventRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class DetailsControllerTests {
    @Autowired
    private MockMvc mvc;

    @Autowired
    EventRepository eventRepository;

    @Test
    public void testExistsEventDetails() throws Exception {
        mvc.perform(get("/event/details/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testNotExistsEventDetails() throws Exception {
        mvc.perform(get("/event/details/1000"))
                .andExpect(status().isOk())
                .andExpect(content().string("{}"));
    }
}
